# Projet compresseur de Huffman en C

## Compilation

```bash
make
```
Les exécutables se trouvent dans le dossier bin
**huf** pour compresser
**dehuf** pour décompresser

## Documentation

```bash
make doc
```
Dans le dossier html se trouve le fichier index.html qui permet de naviguer au travers de toute la documentation

## Utilisation

```bash
./bin/huf <fichier source> <fichier dest>
./bin/dehuf <fichier source>
./archiveur <option> <dossier>
```
