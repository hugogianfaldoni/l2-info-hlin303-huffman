CC          := gcc

CFLAGS      := -O3
LIB         :=
INC         := -Iinc

SOURCES     := 

all: bin/huf bin/dehuf

remake: cleaner all

clean:
	@rm -rf obj/

cleaner: clean
	@rm -rf bin/
	@rm -rf html/

#Link
bin/huf: obj/huf.o obj/pqueue.o obj/btree.o obj/utils.o
	@mkdir -p $(dir $@)
	$(CC) -o bin/huf $^ $(LIB)

bin/dehuf: obj/dehuf.o obj/btree.o obj/utils.o
	@mkdir -p $(dir $@)
	$(CC) -o bin/dehuf $^ $(LIB)

#Compile
obj/%.o: src/%.c
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

doc:
	doxygen doxy.conf

.PHONY: all remake clean cleaner doc
