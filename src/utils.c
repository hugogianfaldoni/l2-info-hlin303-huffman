#include "utils.h"

void write_bit(FILE* file, u8 *buf, u8* left, u8 bit) {
	if (*left == 0) {
		fwrite(buf, sizeof(u8), 1, file);
		*left = 8;
	}

	if (!!bit)
		*buf |= (u8)1 << (*left - 1);
	else
		*buf &= ~((u8)1 << (*left - 1));

	(*left)--;
}

int read_bit(FILE* file, u8 *buf, u8* left) {
	if (*left == 0) {
		fread(buf, sizeof(u8), 1, file);
		*left = 8;
	}

	(*left)--;
	return !!((*buf) & (1 << (*left)));
}
