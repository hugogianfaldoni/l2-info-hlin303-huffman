#include "btree.h"

#include <stdio.h>

#include "utils.h"

static int _print(bt_node_t* node, int level) {
	for (int i = 0; i < level; i++) 
		printf(" ");

	printf("(%+"PRIu8") [%+"PRIu32"]\n", node->symbol, node->count);
	if (node->left)
		_print(node->left, level+1);
	if (node->right)
		_print(node->right, level+1);
}

void btree_print(bt_node_t* root) {
	_print(root, 0);
}
