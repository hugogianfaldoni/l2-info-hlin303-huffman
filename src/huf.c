#include <stdio.h>
#include <stdlib.h>
#include <string.h> // memset

#include "pqueue.h"
#include "huf.h"

int main(int argc, char** argv) {
	if (argc < 3) {
		printf(
				"Utilisation: %s <source> <dest>\n" \
				"   source  est un fichier quelconque à compresser\n" \
				"   dest    est le fichier généré après compression de source",
				argv[0]);
		return 1;
	}

	FILE *source, *dest;
	source = fopen(argv[1], "r");
	dest = fopen(argv[2], "w");

	if (!source) {
		printf("Impossible d'ouvrir le fichier %s en mode lecture\n", argv[1]);
		fclose(source);
		fclose(dest);
		return 2;
	}

	if (!dest) {
		printf("Impossible d'ouvrir le fichier %s en mode écriture\n", argv[2]);
		fclose(source);
		fclose(dest);
		return 3;
	}

	huffman_t data;
	if (lire_fichier(source, &data) < 0) {
		fclose(source);
		fclose(dest);

		// on supprime le fichier dest qui a été crée (il est vide)
		remove(argv[2]);
		return 0;
	}

	printf("Probabilités:\n");
	print_symboles(&data);

	construire_arbre(&data);

	/*
	 * Affiche de l'arbre, demandé dans l'énoncé
	 * Les noeuds internes sont affiché avec le code symbole (0)
	 */
	btree_print(data._root);

	construire_code(&data);

	print_codes(&data);
	printf("Longueur moyenne de codage: %f\n", data._weight_sum);

	ecrire_entete(dest, &data);

	u32 prefix_size = ftell(dest);
	printf("Taille de l'entête: %+"PRIu32" octets\n", prefix_size);

	ecrire_donnees(source, dest, &data);

	u32 dest_size = ftell(dest);
	printf("Taille originelle: %+"PRIu32" octets; ", data.size);
	printf("Taille compressée: %+"PRIu32" octets; ", dest_size);
	printf("Gain: %.1f%%\n", 100.f - dest_size*100.f/data.size);

	free_huffman(&data);

	fclose(source);
	fclose(dest);

	return 0;
}

int lire_fichier(FILE* source, huffman_t* data) {
	memset(data->symbol_count, 0, 256*sizeof(*data->symbol_count));
	memset(data->weights, 0, 256*sizeof(*data->weights));
	data->unique_symbols = 0;

	// on compte le nombre d'occurance de chaque symboles
	u8 symbol = 0;
	while (fread(&symbol, sizeof(symbol), 1, source) && !feof(source))
		data->symbol_count[symbol]++;

	// on va la fin du fichier et on sauvegarde sa taille
	fseek(source, 0, SEEK_END);
	data->size = ftell(source);

	if (data->size == 0) {
		fprintf(stderr, "Fichier source vide\n");
		return -1;
	}

	for (int i = 0; i < 256; i++) {
		// si le symbole n'est pas dans le fichier source, on le passe
		if (data->symbol_count[i] == 0)
			continue;
		double w = (double)data->symbol_count[i]/(double)data->size;
		data->weights[i] = w;
		data->unique_symbols++;
	}

	return 0;
}

void print_symboles(huffman_t* data) {
	for (int i = 0; i < 256; i++) {
		if (data->symbol_count[i] == 0)
			continue;
		printf("symbole(%+"PRIu8") = %f\n", (u8)i, data->weights[i]);
	}
	printf("Nombre de symboles: %d\n", data->unique_symbols);
}

void construire_arbre(huffman_t* data) {
	pqueue_t* queue = pqueue_new();
	btree_t* btree = &data->_btree;

	/*
	 * Un arbre binaire complet de taille n possède n+n-1 noeuds
	 *
	 * Cas particulier: si le fichier source ne contient qu'un seul symbole
	 * on doit construire un arbre avec un feuille et une racine donc 2 cases.
	 *
	 * on rajoute une case pour l'arbre dans tous les cas, la perte de mémoire est négligable
	 */
	data->_root = NULL;
	btree->nodes = calloc(
			data->unique_symbols+(data->unique_symbols-1)+1,
			sizeof(*btree->nodes));

	memset(data->_itable, 0, 256);

	// on insert tous les symboles en tant que feuille de l'arbre
	for (int i = 0, j = 0; i < 256; i++) {
		// on passe les symboles vides
		if (data->symbol_count[i] == 0)
			continue;

		// on les place dans l'arbre
		btree->nodes[j].symbol = (u8)i;
		btree->nodes[j].count = data->symbol_count[i];
		data->_itable[i] = j;

		// et dans la file
		pqueue_insert(queue, &btree->nodes[j]);
		j++;
	}

	/*
	 * Maintenant on contruit l'arbre:
	 *   - prendre les 2 noeuds qui ont le poids le plus faible
	 *   - les enlever de la file
	 *   - créer un nouveau noeud dont le poids est la somme des 2 noeuds précédent
	 *   - insérer ce nouveau noeud dans l'arbre avec comme fils les 2 noeuds précédent
	 *   - insérer ce nouveau noeud dans la file
	 *   - répéter jusqu'à ce que la file soit vide
	 */
	btree->ipos = data->unique_symbols;
	while (queue->size > 1) {
		bt_node_t* n1 = pqueue_pop(queue);
		bt_node_t* n2 = pqueue_pop(queue);

		btree->nodes[btree->ipos].symbol = 0; // le symbole n'est pas important pour les noeuds internes
		btree->nodes[btree->ipos].count = n1->count + n2->count;
		btree->nodes[btree->ipos].left = n1;
		btree->nodes[btree->ipos].right = n2;

		n1->parent = &btree->nodes[btree->ipos];
		n2->parent = &btree->nodes[btree->ipos];

		// on insert ce nouveau noeud dans la file
		pqueue_insert(queue, &btree->nodes[btree->ipos]);
		data->_root = &btree->nodes[btree->ipos];
		btree->ipos++;
	}

	/*
	 * Si en sortant de la boucle et que data->_root=NULL c'est que le fichier source ne contient
	 * qu'un seul symbole qui est répété plusieurs fois
	 */
	if (data->_root == NULL) {
		// on crée le noeud racine
		data->_root = &btree->nodes[1];
		data->_root->symbol = 0;
		data->_root->count = btree->nodes[0].count;
		data->_root->left = &btree->nodes[0];
		data->_root->right = NULL;

		btree->nodes[0].parent = data->_root;
	}

	pqueue_destroy(queue);
}

void construire_code(huffman_t* data) {
	/*
	 * Une fois que l'arbre est crée on peut construire le code de chaque symboles
	 *
	 * On a besoin de 2 tableaux:
	 *  - un pour stocker le code
	 *  - un autre pour stocker la longueur du code
	 *
	 * En effet on ne connait pas le nombre de bits utilisés sur le u32 pour stocker le code
	 *
	 * Pour construire le code on prend les feuilles et on remonte jusqu'à la
	 * racine de l'arbre en assignant 1 si on est le fils droit, sinon 0
	 */
	data->_code = calloc(data->unique_symbols, sizeof(u32));
	data->_code_size = calloc(data->unique_symbols, sizeof(u32));
	data->_max_code_size = 0;

	// les feuilles sont stockées dans les total_symbols premières cases
	data->_weight_sum = 0.0;
	for (int i = 0; i < data->unique_symbols; i++) {
		bt_node_t* node = &data->_btree.nodes[i];
		int csize = 0;
		int cindex = data->_itable[(int)node->symbol];
		int symbol = node->symbol;

		while (node->parent) {
			data->_code_size[cindex]++;
			data->_code[cindex] >>= 1;
			if (node->parent->right == node)
				data->_code[cindex] |= (1 << (sizeof(*data->_code)*8-1));
			node = node->parent;
		}
		data->_code[cindex] >>= sizeof(*data->_code)*8 - data->_code_size[cindex];
		data->_weight_sum += data->weights[symbol]*data->_code_size[cindex];
		if (data->_code_size[cindex] > data->_max_code_size)
			data->_max_code_size = data->_code_size[cindex];
	}
}

void print_codes(huffman_t* data) {
	char* buffer = calloc(data->_max_code_size + 1, sizeof(char));
	buffer[data->_max_code_size] = '\0';

	/*
	 * On regarde tous les symboles, on affiche le de ceux qui ont
	 * une longueur de code > 0
	 */
	for (int i = 0; i < 256; i++) {
		int cindex = data->_itable[i];

		if (data->symbol_count[i] == 0)
			continue;
		// on remplit le buffer d'espaces
		memset(buffer, ' ', data->_max_code_size);
		// on écrit le code au bon endroit dans le buffer
		// les codes sont allignés à droite
		for (int j = 0; j < data->_code_size[cindex]; j++) {
			int bit = data->_code[cindex] & (1 << j);
			buffer[data->_max_code_size - 1 - j] = bit ? '1' : '0';
		}

		printf("code(%.3d) = %s\n", i, buffer);
	}

	free(buffer);
}

void ecrire_entete(FILE* dest, huffman_t* data) {
	/*
	 * On écrit dans le fichier de sortie la taille du fichier source
	 * Cela permet de savoir quand arreter la lecture des codes quand
	 * on décompresse le fichier
	 */
	fwrite(&data->size, sizeof(data->size), 1, dest);

	/*
	 * Maintenant il ne reste plus qu'a écrire les codes dans le fichier de sortie
	 *
	 * Avant cela, il faut écrire les informations nécessaire au décodeur. Une méthode simple
	 * et efficace est d'écrire la structure de l'arbre et les symboles.
	 *
	 * On représente un noeud interne par un 0, une feuille par un 1
	 * Lorqu'on écrit une feuille, on écrit le 1 suivit des 8 bits du symbole.
	 *
	 * On commence par écrire le noeud actuel puis le gauche et droit
	 */

	struct stack {
		bt_node_t* node;
		struct stack* prev;
	};
	struct stack head;
	head.node = NULL;
	head.prev = NULL;

	struct stack* cur = calloc(1, sizeof(struct stack));
	cur->node = data->_root;
	cur->prev = &head;

	data->_buf = 0;
	data->_bsize = 8;
	int k = 0;
	while (cur->prev) {
		struct stack* prev = cur->prev;
		bt_node_t* n = cur->node;
		free(cur);
		cur = NULL;

		// feuille donc on écrit 1 + symbole
		if (n->left == NULL && n->right == NULL) {
			write_bit(dest, &data->_buf, &data->_bsize, 1);
			for (int i = 7; i >= 0; i--) {
				write_bit(dest, &data->_buf, &data->_bsize, n->symbol & (1 << i));
			}
			cur = prev;
		}
		else { // sinon on écrit un 0
			write_bit(dest, &data->_buf, &data->_bsize, 0);
			if (n->right) {
				cur = malloc(sizeof(struct stack));
				cur->prev = prev;
				cur->node = n->right;
				prev = cur;
			}
			if (n->left) {
				cur = malloc(sizeof(struct stack));
				cur->prev = prev;
				cur->node = n->left;
			}
		}
	}
}

void ecrire_donnees(FILE* source, FILE* dest, huffman_t* data) {
	/*
	 * Maintenant qu'on a écrit l'arbre, on retourne au début du fichier source
	 * pour lire les symboles et écrire leur code dans le fichier dest
	 */
	rewind(source);

	u8 s;
	while (fread(&s, sizeof(u8), 1, source) && !feof(source)) {
		int cindex = data->_itable[s];
		for (int i = data->_code_size[cindex] - 1; i >= 0; i--) {
			if (data->_code[cindex] & (1 << i)) {
				write_bit(dest, &data->_buf, &data->_bsize, 1);
			}
			else {
				write_bit(dest, &data->_buf, &data->_bsize, 0);
			}
		}
	}

	/*
	 * S'il reste des données dans le buffer, on remplit de 0
	 * et on écrit dans le fichier dest
	 */
	while (data->_bsize != 0) {
		write_bit(dest, &data->_buf, &data->_bsize, 0);
	}
	fwrite(&data->_buf, sizeof(data->_buf), 1, dest);
}

void free_huffman(huffman_t* data) {
	free(data->_btree.nodes);
	free(data->_code);
	free(data->_code_size);
}
