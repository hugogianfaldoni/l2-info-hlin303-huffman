#include <stdio.h>
#include <stdlib.h>

#include "dehuf.h"

int main(int argc, char** argv) {
	if (argc < 2) {
		fprintf(
				stderr,
				"Utilisation: %s <source>\n" \
				"   source  est un fichier quelconque à décompresser\n" \
				"\n" \
				"   Le fichier décompressé est affiché sur la sortie standard\n", 
				argv[0]);
		return 1;
	}

	FILE *source;
	source = fopen(argv[1], "r");

	if (!source) {
		fprintf(stderr, "Impossible d'ouvrir le fichier %s en mode lecture\n", argv[2]);
		fclose(source);

		return 2;
	}

	dehuffman_t data;
	lire_entete(source, &data);

	/*
	 * Une fois qu'on a l'arbre de Huffman, il ne reste plus qu'a lire les données 
	 * du fichier source pour déterminer les symboles
	 */
	decompresser(source, &data, stdout);

	free_huffman(&data);
	fclose(source);

	/*
	 * Si le nombre de symboles lus est différent de la taille du fichier source
	 * c'est qu'on a atteind la fin du fichier compressé avant d'avoir tout lu
	 */
	if (data.read_symbols != data.size) {
		fprintf(stderr, "Erreur lors de la lecture du fichier compressé\n");
		return 1;
	}

	return 0;
}

void lire_entete(FILE* source, dehuffman_t* data) {
	// on lit la taille du fichier
	fread(&data->size, sizeof(data->size), 1, source);

	// on ne connait pas le nombre de symbole, on alloue le max
	data->_btree.nodes = calloc(256+(256-1), sizeof(*data->_btree.nodes));
	data->_btree.ipos = 0;

	int left = 1;
	data->_buf = 0;
	data->_bsize = 0;

	// on lit le noeud racine
	bt_node_t* prev = &data->_btree.nodes[data->_btree.ipos++];
	bt_node_t* cur = NULL;
	read_bit(source, &data->_buf, &data->_bsize);

	int read_tree = 1;
	while (read_tree) {
		int bit = read_bit(source, &data->_buf, &data->_bsize);
		// si on a un noeud
		if (bit == 0) {
			cur = &data->_btree.nodes[data->_btree.ipos++];
			cur->parent = prev;
			if (left) {
				prev->left = cur;
			}
			else {
				prev->right = cur;
				left = 1;
			}
			prev = cur;
		}
		// si c'et une feuille
		else {
			u8 symbol;
			for (int i = 0; i < 8; i++) {
				symbol <<= 1;
				bit = read_bit(source, &data->_buf, &data->_bsize);
				symbol |= bit;
			}
			bt_node_t* child = &data->_btree.nodes[data->_btree.ipos++];
			child->symbol = symbol;
			if (left) {
				prev->left = child;
				child->parent = prev;
				left = 0;

				/*
				 * Si on insert une feuille à gauche et que le noeud parent et la racine
				 * c'est qu'on a qu'une seule feuille dans notre arbre
				 */
				if (prev == &data->_btree.nodes[0])
					read_tree = 0;
			}
			else {
				prev->right = child;
				child->parent = prev;
				while (prev->right != NULL && prev != &data->_btree.nodes[0])
					prev = prev->parent;
				if (prev == &data->_btree.nodes[0] && prev->right != NULL)
					read_tree = 0;
			}
		}
	}
}

void decompresser(FILE* source, dehuffman_t* data, FILE* output) {
	bt_node_t* cur = &data->_btree.nodes[0];
	data->read_symbols = 0;

	while (data->read_symbols < data->size && !feof(source)) {
		int bit = read_bit(source, &data->_buf, &data->_bsize);
		if (bit) {
			cur = cur->right;
		}
		else {
			cur = cur->left;
		}
		// si on atteind une feuille on a décodé un symbole
		if (cur->right == NULL && cur->left == NULL) {
			// on écrit le symbole sur la sortie standard
			fprintf(output, "%c", cur->symbol);
			// et on retourne à la racine
			cur = &data->_btree.nodes[0];
			data->read_symbols++;
		}
	}
}

void free_huffman(dehuffman_t* data) {
	free(data->_btree.nodes);
}
