#include "pqueue.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

pqueue_t* pqueue_new() {
	pqueue_t* q = malloc(sizeof *q);
	q->size = 0;
	q->head = NULL;
	q->tail = NULL;

	return q;
}

void pqueue_destroy(pqueue_t* q) {
	if (!q)
		return;

	pq_node_t* next = q->head;
	while (next) {
		pq_node_t* tmp = next->next;
		free(next);
		next = tmp;
	}

	free(q);
}

void pqueue_insert(pqueue_t* q, bt_node_t* value) {
	if (!q)
		return;
	assert(value);

	pq_node_t* tmp = malloc(sizeof *tmp);
	tmp->data = value;
	tmp->next = NULL;

	if (q->size == 0) {
		q->head = q->tail = tmp;
	}
	else {
		pq_node_t *prev = NULL;
		pq_node_t *next = q->head;

		/*
		 * Algorithm de recherche de position O(n)
		 * Le nombre d'insertion n'est pas lié à la taille du fichier 
		 * mais au nombre de symbole différents qui est de 256 maximum
		 *
		 * inutile de créer un algorithme de recherche plus complexe
		 */
		while (next != NULL && next->data->count < value->count) {
			prev = next;
			next = next->next;
		}

		if (prev == NULL) { // ajout en tête
			tmp->next = q->head;
			q->head = tmp;
		}
		else if (next == NULL) { // ajout en fin
			tmp->next = NULL;
			q->tail->next = tmp;
			q->tail = tmp;
		}
		else { // ajout au milieu
			tmp->next = next;
			prev->next = tmp;
		}
	}

	q->size++;
}

bt_node_t* pqueue_pop(pqueue_t* q) {
	assert(q && q->size > 0);

	pq_node_t* tmp = q->head;
	bt_node_t* ret = tmp->data;

	q->size--;
	if (q->size == 0) {
		q->head = q->tail = NULL;
	}
	else {
		q->head = tmp->next;
	}
	free(tmp);
	return ret;
}

void pqueue_print(pqueue_t* q) {
	if (!q || q->size == 0)
		return;

	pq_node_t* tmp = q->head;
	while (tmp != q->tail) {
		printf("(%c: %lu) -> ", tmp->data->symbol, tmp->data->count);
		tmp = tmp->next;
	}
	printf("(%c: %lu)\n", q->tail->data->symbol, q->tail->data->count);
}
