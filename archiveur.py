#!/bin/python2

from os import walk, listdir, remove
from os.path import isfile, join, isdir
from subprocess import call, PIPE
from sys import argv

def get_files_in_dir(directory):
    files = [f for f in listdir(directory) if isfile(join(directory, f))]
    return files

def get_dirs_in_dir(directory):
    dirs = [d for d in listdir(directory) if isdir(join(directory, d))]
    return dirs

def compresser(dir_in):
    files = get_files_in_dir(dir_in)
    for f in files:
        file_path = join(dir_in, f)
        if call(["./bin/huf", file_path, file_path + ".huf"]) == 0:
            remove(file_path)
    dirs = get_dirs_in_dir(dir_in)
    for d in dirs:
        compresser(join(dir_in, d))

def decompresser(dir_in):
    files = get_files_in_dir(dir_in)
    for f in files:
        file_path = join(dir_in, f)
        if file_path[-4:] == ".huf":
            file_out = open(file_path[:-4], "w")
            if call(["./bin/dehuf", file_path], stdout=file_out) == 0:
                remove(file_path)
    dirs = get_dirs_in_dir(dir_in)
    for d in dirs:
        decompresser(join(dir_in, d))

if len(argv) < 3:
    print("Utilisation:", argv[0], "[-c|-d] [dir]")
    exit(1)

if argv[1] == "-c":
    compresser(argv[2])
elif argv[1] == "-d":
    decompresser(argv[2])
else:
    print("Option inconnue", argv[1])
    exit(1)
