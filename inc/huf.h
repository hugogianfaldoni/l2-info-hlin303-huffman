/**
 * @file huf.h
 * @author Hugo Gianfaldoni
 * @brief Contient les fonctions necessaire à la compression d'un fichier
 */

#ifndef DEF_HUF_H
#define DEF_HUF_H

#include "btree.h"
#include "utils.h"

/**
 * @brief Structure de donnée utilisée par l'ensemble des fonctions servant à construire 
 *        le code de huffman d'un fichier source
 *
 * Structure de données utilisée uniquement pour la compression
 */
struct huffman {
	u32 symbol_count[256]; ///< nombre d'apparition de chaque symbole
	u32 unique_symbols;    ///< nombre de symboles apparaissant au moins une fois dans le fichier source
	u32 size;              ///< taille du fichier source
	double weights[256];   ///< proba de chaque symbole

	// private
	btree_t _btree;    // arbre binaire contenant le code de huffman
	bt_node_t* _root;  // racide de l'arbre
	int _itable[256];  // index des symboles dans le tableau de données de l'arbre
	u32* _code;         // code de huffman des symboles
	u32* _code_size;    // longueur des codes
	u32  _max_code_size; // longueur du code le plus long, utilisé dans print_codes
	double _weight_sum; // somme des probas
	u8 _buf, _bsize;   // buffer utilisé pour écrire bit par bit dans le fichier destination
};
typedef struct huffman huffman_t;

/**
 * @brief Lit le fichier source
 *
 * Lit le fichier source et remplit:
 *    data->symbol_count, 
 *    data->weights,
 *    data->unique_symbols,
 *    data->size
 *
 * Cette fonction calcule les probabilitées des symboles, le nombre d'occurance de chaque symboles,
 * le nombre de symboles uniques et le nombre total de syboles dans le fichier source.
 *
 * @param source le fichier source qui doit être ouvert en mode lecture
 * @param data structure de données à remplir
 *
 * @return 0 si il n'y a pas d'erreurs
 * @return -1 si le fichier source est vide
 */
int lire_fichier(FILE* source, huffman_t* data);

/**
 * @brief Affiche les symboles avec leurs probabilité
 *
 * @param data structure de données qui contient les symboles
 */
void print_symboles(huffman_t* data);

/**
 * @brief Construit l'arbre de huffman
 *
 * Une fois la liste des symboles et leurs proba obtenue, on peut construire
 * l'arbre de huffman pour le fichier source.
 *
 * Remplit data->_btree et data->_root
 *
 * data->_root est un pointeur vers la racine de l'arbre binaire
 *
 * @param data structure de données avec toutes les infos necessaires pour construire l'arbre
 */
void construire_arbre(huffman_t* data);

/**
 * @brief Construit le code de chaque symbole à partir de l'arbre
 *
 * Une fois l'arbre obtenu, on peut construire le code de chaque symbole.
 * Remplit data->code et data->code_size
 *
 * Remplit aussi data->_weight_sum
 *
 * @param data structure de données contenant l'arbre de huffman
 */
void construire_code(huffman_t* data);

/**
 * @brief affiche les codes des symboles
 *
 * @param data structure de données qui contient les codes
 */
void print_codes(huffman_t* data);

/**
 * @brief écrit l'entete necessaire pour la décompression des données
 *
 * Une fois que les codes sont construits, on peut écrire l'entete dans
 * le fichier destination.
 * Le fichier destination doit être ouvert en mode écriture.
 *
 * L'entete contient la taille du fichier source (un u32) et l'arbre de huffman
 * encodé avec un 0 pour un noeud et un 1 suivit des 8 bits du symbole pour une feuille
 *
 * Cela permet d'avoir la taille minimum possible pour l'entete.
 *
 * @param dest fichier destination ouvert en mode écriture
 * @param data structure de donnée qui contient les codes
 */
void ecrire_entete(FILE* dest, huffman_t* data);

/**
 * @brief Lit les symboles du fichier source et écrit le code du symbole dans
 * le fichier dest
 *
 * @param source le fichier source ouvert en mode lecture
 * @param dest le fichier dest ouvert en mode écriture
 * @param data structure de donnée qui contient les codes
 */
void ecrire_donnees(FILE* source, FILE* dest, huffman_t* data);

/**
 * @brief Libère la mémoire utilisée dans la structure de donnée
 *
 * @param data structure de donnée à libérer
 */
void free_huffman(huffman_t* data);

#endif
