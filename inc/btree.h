/**
 * @file btree.h
 * @author Hugo Gianfaldoni
 * @brief Contient les structures et fonctions de gestion pour un arbre binaire
 */

#ifndef DEF_BTREE_H
#define DEF_BTREE_H

#include "utils.h"

/**
 * @brief Noud d'un arbre binaire
 *
 * Un noeud d'un arbre binaire avec des pointeurs vers les fils droit et gauche
 * et vers le noeud parent
 *
 * Les données sont le symbole du noeud et le nombre d'occurance 
 * de ce symbole dans le fichier source
 */
struct bt_node {
	struct bt_node* left; ///< fils gauche
	struct bt_node* right; ///< fils droit
	struct bt_node* parent; ///< noeud parent

	u8 symbol; ///< symbole du noeud (-1) si ce n'est pas une feuille
	u32 count; ///< poids du noeud
};
typedef struct bt_node bt_node_t;

/**
 * @brief Simple structure pour stocker un arbre binaire
 */
struct btree {
	bt_node_t* nodes; ///< nodes contient tous les noeuds et doit être initialisé et géré "à la main"
	u32 ipos; ///< position d'insertion du prochain noeud
};
typedef struct btree btree_t;

/**
 * @brief Affiche l'arbre binaire dont la racine est root
 */
void btree_print(bt_node_t* root);

#endif // DEF_BTREE_H
