/**
 * @file pqueue.h
 * @author Hugo Gianfaldoni
 * @brief Contient les structures et fonctions de gestion pour un file en priorité
 */
#ifndef DEF_PQUEUE_H
#define DEF_PQUEUE_H

#include <stddef.h>

#include "btree.h"

/**
 * @brief Noeud d'une file en priorité
 *
 * Une noeud contenant ses données et le prochain noeud dans la file
 */
struct pq_node {
	struct pq_node* next; ///< Le prochain noeud dans la file
	bt_node_t* data; ///< Données associée au noeud
};
typedef struct pq_node pq_node_t;

/**
 * @brief Structure pour stocker une file en priorité
 */
struct pqueue {
	pq_node_t* head; ///< Tête de la file
	pq_node_t* tail; ///< Queue de la file

	size_t size; ///< Nombre d'élements dans la file
};
typedef struct pqueue pqueue_t;

/**
 * @brief Créer une nouvelle file
 * @return la nouvelle file
 */
pqueue_t*  pqueue_new();
/**
 * @brief Supprime une file
 *
 * Déalloue la mémoire de tous les éléments de la file
 */
void       pqueue_destroy(pqueue_t* q);

/**
 * @brief Insert un élement dans la file
 *
 * Si la file est NULL, ne fait rien.
 * Les élements sont triés par ordre croissant
 */
void       pqueue_insert(pqueue_t* q, bt_node_t* value);

/**
 * @brief supprime et retourne le premier élement de la file
 *
 * Si la file est vide, l'assert fail.
 *
 * @return le premier élément
 */
bt_node_t* pqueue_pop(pqueue_t* q);

/**
 * @brief Affiche la file
 *
 * Si la file est NULL ou vide, n'affiche rien.
 */
void       pqueue_print(pqueue_t* q);


#endif // DEF_PQUEUE_H
