/** 
 * @file utils.h
 * @author Hugo Gianfaldoni
 * @brief Contient les fonctions et typedefs utilitaires.
 */

#ifndef DEF_UTILS_H
#define DEF_UTILS_H

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

typedef uint8_t u8;   ///< Entier non signé sur 8 bits
typedef uint16_t u16; ///< Entier non signé sur 16 bits
typedef uint32_t u32; ///< Entier non signé sur 32 bits
typedef uint64_t u64; ///< Entier non signé sur 64 bits

/**
 * @brief Écrit dans le fichier file bit par bit
 *
 * Cette fonction écrit dans le buffer buf jusqu'à ce qu'il soit
 * plein. Une fois qu'il est plein, le prochain appel à la fonction
 * va écrire le buffer dans le fichier.
 *
 * Lors du premier appel à la fonction, left doit être égal à 8
 * Pour les appels suivant il ne faut pas modifier buf ou left
 *
 * @param file un fichier ouvert en écriture
 * @param buf un pointeur vers un u8
 * @param left un pointeur vers un u8
 * @param bit 0 ou 1, le bit à écrire
 */
void write_bit(FILE* file, u8 *buf, u8* left, u8 bit);

/**
 * @brief Lit depuis le fichier file bit par bit
 *
 * Cette fonction lit depuis le buffer tant qu'il est plein. Une fois
 * qu'il est vide, le prochain appel à la fonction va lire un octet et
 * écrire dans le buffer
 *
 * Lors du premier appel à la fonction, *left doit être égal à 0
 * Pour les appels suivant il ne faut pas modifier buf ou left
 *
 * @param file un fichier ouvert en lecture
 * @param buf un pointeur vers un u8
 * @param left un pointeur vers un u8
 */
int read_bit(FILE* file, u8 *buf, u8 *left);

#endif
