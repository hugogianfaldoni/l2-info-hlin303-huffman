/**
 * @file dehuf.h
 * @author Hugo Gianfaldoni
 * @brief Contient les fonctions necessaire à la décompression d'un fichier
 */

#ifndef DEF_DEHUF_H
#define DEF_DEHUF_H

#include "btree.h"
#include "utils.h"

/**
 * @brief Structure de donnée utilisée par l'ensemble des fonctions servant à
 * stocker l'arbre binaire lu depuis le fichier source
 *
 * Struture de donnée utilisée uniquement pour la décompression
 */
struct dehuffman {
	u32 size; ///< taille du fichier décompressé
	u32 read_symbols; ///< nombre de symboles décompressés

	// private
	btree_t _btree; // arbre binaire contenant le code de huffman
	u8 _buf, _bsize; // buffer utilisé pour lire le fichier bit par bit
};
typedef struct dehuffman dehuffman_t;

/**
 * @brief Lit l'entete dans le fichier source
 *
 * Depuis le fichier source, lit:
 *    data->size
 *    data->_btree
 * 
 * Decode la structure de l'arbre binaire et le recréer
 *
 * @param source fichier source ouvert en mode lecture
 * @param data structure de donnée à remplir
 */
void lire_entete(FILE* source, dehuffman_t* data);

/**
 * @brief Décompresse le fichier source
 *
 * Une fois que l'entete est lue, lit le reste des données dans le fichier source
 * et écrit les symboles décompressés dans output.
 *
 * @param source fichier source ouvert en mode lecture
 * @param data structure de données qui contient l'arbre de huffman
 * @param output fichier destination ouvert en mode écriture
 */
void decompresser(FILE* source, dehuffman_t* data, FILE* output);

/**
 * @brief Libère la mémoire utilisée dans la structure de donnée
 *
 * @param data structure de donnée à libérer
 */
void free_huffman(dehuffman_t* data);

#endif
